<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->id();
            
            $table->bigInteger('user_id', false, true)->nullable(true);
            $table->string('category', 50);
            $table->text('complaint');
            $table->boolean('resolved')->default(false);
            
            $table->string('phone', 15)->nullable(true);
            $table->string('email')->nullable(true);
            $table->integer('reply_count', false, true)->default(0);
            $table->bigInteger('last_reply_user_id', false, true)->nullable(true);
            
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users')
            ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
}
