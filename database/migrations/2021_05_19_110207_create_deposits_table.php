<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposits', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->bigInteger('transaction_id', false, true);
            $table->string('depositor_name')->nullable(true);
            $table->string('channel', 60);
            $table->string('payment_method', 60)->nullable(true);
//             $table->date('payment_date')->nullable(true);
            $table->string('reference')->nullable(true);
            
            $table->timestamps();
            
            $table->foreign('transaction_id')->references('id')->on('transactions')
                ->onDelete('cascade')->onUpdate('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposits');
    }
}
