<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_subjects', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->string('exam_no', 20);
            $table->bigInteger('course_id', false, true)->nullable(true);
            $table->bigInteger('course_session_id', false, true)->nullable(true);
            $table->integer('score', false, true)->nullable(true);
            $table->integer('num_of_questions', false, true)->nullable(true);
            $table->string('status')->default(STATUS_ACTIVE);
            
            $table->foreign('exam_no')->references('exam_no')->on('exams')
            ->onDelete('cascade')->onUpdate('cascade');
            
            $table->foreign('course_id')->references('id')->on('courses')
            ->onDelete('cascade')->onUpdate('cascade');
            
            $table->foreign('course_session_id')->references('id')->on('course_sessions')
            ->onDelete('cascade')->onUpdate('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_subjects');
    }
}
