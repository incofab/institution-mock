<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->bigInteger('institution_id', false, true);
            $table->string('title');
            $table->string('description')->nullable(true);
            $table->string('duration', 10);
            $table->string('status')->default('active');
            $table->integer('num_of_activations', false, true)->default(0);
            
            $table->timestamps();
            
            $table->foreign('institution_id')->references('id')->on('institutions')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
