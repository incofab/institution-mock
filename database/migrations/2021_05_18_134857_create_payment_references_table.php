<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_references', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            
            $table->bigInteger('user_id', false, true)->nullable(true);
            $table->string('reference')->unique();
            $table->string('merchant', 30)->default(MERCHANT_PAYSTACK);
            $table->bigInteger('payment_id', false, true)->nullable(true);
            $table->string('payment_type')->nullable(true);
            $table->float('amount')->default(0);
            $table->float('charge')->default(0);
            $table->text('content')->nullable(true);
            $table->string('status', 30)->default(STATUS_PENDING);
            
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_references');
    }
}
