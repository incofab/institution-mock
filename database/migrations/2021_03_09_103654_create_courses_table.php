<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->bigInteger('institution_id', false, true)->nullable(true);
            
            $table->string('course_code', 80);
            $table->string('category', 50)->nullable(true);
            $table->string('course_title')->nullable(true);
            $table->text('description')->nullable(true);
            
            $table->boolean('is_file_content_uploaded')->default(false);
            
            $table->timestamps();
            
            $table->foreign('institution_id')->references('id')->on('institutions')
                ->onDelete('cascade')->onUpdate('cascade');			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
