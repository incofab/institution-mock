<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            
            $table->bigInteger('user_id', false, true)->nullable(true);
            
            $table->float('amount');
            
            $table->string('transaction_type', 50);
            
            $table->string('ip_address', 25)->nullable(true);
            
            $table->string('choice_platform', 15)->nullable(true);
            
            $table->string('transaction_entry', 50);
            
            $table->string('status', 50)->default(STATUS_PENDING);
            
            $table->float('bbt')->nullable(true);
            $table->float('bat')->nullable(true);
            $table->float('charge')->default(0);
            
            $table->text('comment')->nullable(true);
            $table->string('reference')->unique();
            
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
                        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
