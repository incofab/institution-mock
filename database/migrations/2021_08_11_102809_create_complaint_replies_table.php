<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplaintRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaint_replies', function (Blueprint $table) {
            
            $table->id();
            
            $table->bigInteger('complaint_id', false, true);
            $table->bigInteger('user_id', false, true)->nullable(true);
            $table->text('message');
            $table->timestamps();
            
            $table->foreign('complaint_id')->references('id')->on('complaints')
            ->onDelete('cascade')->onUpdate('cascade');
            
            $table->foreign('user_id')->references('id')->on('users')
            ->onDelete('cascade')->onUpdate('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaint_replies');
    }
}
