<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservedAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserved_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->bigInteger('user_id', false, true)->nullable();
            $table->string('reference')->unique();
            $table->string('status', 20)->default(STATUS_INACTIVE);
            $table->string('merchant', 20)->default(MERCHANT_PAYSTACK);
            $table->string('account_name', 120)->nullable(true);
            $table->string('bank_name', 100)->nullable(true);
            $table->string('account_number', 20)->nullable(true);
            
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserved_accounts');
    }
}
