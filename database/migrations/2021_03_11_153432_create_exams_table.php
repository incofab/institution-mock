<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->bigInteger('event_id', false, true)->nullable(true);
            $table->string('exam_no', 20)->unique();
            $table->string('student_id')->nullable(true);
            
            $table->string('duration', 10);
            $table->float('time_remaining');
            
            $table->dateTime('start_time')->nullable(true);
            $table->dateTime('pause_time')->nullable(true);
            $table->dateTime('end_time')->nullable(true);
            $table->integer('score', false, true)->nullable(true);
            $table->integer('num_of_questions', false, true)->nullable(true);
            $table->string('status')->default(STATUS_ACTIVE);
                        
            $table->timestamps();
            
            $table->foreign('student_id')->references('student_id')->on('students')
            ->onDelete('cascade')->onUpdate('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams');
    }
}
