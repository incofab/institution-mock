<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institutions', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            
            $table->bigInteger('added_by', false, true)->nullable(true);
            $table->string('code', 10)->unique();
            $table->string('name', 255);
            $table->string('address')->nullable(true);
            $table->string('phone', 15)->nullable(true);
            $table->string('email', 150)->nullable(true);
            $table->string('status')->default('active');
            
            $table->timestamps();
            
        });
        
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institutions');
    }
}
