<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_subjects', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->bigInteger('event_id', false, true);
            $table->bigInteger('course_id', false, true)->nullable(true);
            $table->bigInteger('course_session_id', false, true)->nullable(true);
            
            $table->string('status')->default('active');
            
            $table->timestamps();
            
            $table->foreign('event_id')->references('id')->on('events')
            ->onDelete('cascade')->onUpdate('cascade');
            
            $table->foreign('course_id')->references('id')->on('courses')
            ->onDelete('cascade')->onUpdate('cascade');
            
            $table->foreign('course_session_id')->references('id')->on('course_sessions')
            ->onDelete('cascade')->onUpdate('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_subjects');
    }
}
