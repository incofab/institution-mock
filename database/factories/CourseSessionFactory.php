<?php
namespace Database\Factories;

use Faker\Generator as Faker;
use App\Models\CourseSession;
use Illuminate\Database\Eloquent\Factories\Factory;

class CourseSessionFactory extends Factory
{
    function definition() {
        $couseIDs = \App\Models\Course::all('id')->pluck('id')->toArray();
        $sessions = ['2001', '2002', '2003', '2004', '2005', '2006'];
        
        return [
            'course_id' => fake()->randomElement($couseIDs), 
            'category' => '', 
            'session' => fake()->randomElement($sessions)
        ];
    }
}
