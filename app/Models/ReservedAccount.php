<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
/**
 * @author Incofab
 * @property int $id
 * @property int $user_id
 * @property string $reference
 * @property string $status
 * @property string $merchant
 * @property string $account_name
 * @property string $account_number
 * @property string $bank_name
 */
class ReservedAccount extends BaseModel
{
    protected $table = 'reserved_accounts';
    
    public $fillable = ['id', 'user_id', 'reference', 'status', 'merchant',
        'account_name', 'account_number', 'bank_name'];
    
    static function ruleCreate() {
        return [];
    }
    
    static function ruleUpdate() {
        return [];
    }
    
    static function insert($userId, $merchant, $post = [])
    {
        $arr = [];
        $arr['reference'] = self::generateReferece();
        $arr['user_id'] = $userId;
        $arr['merchant'] = $merchant;
        
        $arr['account_name'] = Arr::get($post, 'account_name');
        $arr['account_number'] = Arr::get($post, 'account_number');
        $arr['bank_name'] = Arr::get($post, 'bank_name');
        $arr['status'] = Arr::get($post, 'status', STATUS_PENDING);
        
        $data = self::create($arr);
        
        if ($data)
        {
            return [SUCCESSFUL => true, MESSAGE => 'Data recorded', 'data' => $data];
        }
        
        return [SUCCESSFUL => false, MESSAGE => 'Error: Data entry failed'];
    }
    
    static function edit($post)
    {
        $val = \Illuminate\Support\Facades\Validator::make($post, self::ruleCreate());
        
        if ($val->fails())
        {
            return [SUCCESSFUL => false, MESSAGE => 'Validation failed', 'val' => $val->errors() ];
        }
        
        $editRecord = self::where('reference', '=', $post['reference'])->first();
        
        if(!$editRecord)
        {
            return [SUCCESSFUL => false, MESSAGE => 'Record not found'];
        }
        
        $editRecord['account_name'] = $post['account_name'];
        $editRecord['account_number'] = $post['account_number'];
        $editRecord['bank_name'] = $post['bank_name'];
        $editRecord['status'] = $post['status'];
        
        if($editRecord->save())
        {
            return [SUCCESSFUL => true, MESSAGE => 'Record updated successfully', 'data' => $editRecord];
        }
        
        return [SUCCESSFUL => false, MESSAGE => 'Data entry failed'];
    }
    
    static private function generateReferece()
    {
        $reference = \App\Core\CodeGenerator::generateCodes(15);
        
        while (static::where('reference', '=', $reference)->first())
        {
            $reference = \App\Core\CodeGenerator::generateCodes(15);
        }
        
        return $reference;
    }
    
    static function getUserReservedAccount($userId)
    {
        return static::where('user_id', '=', $userId)->where('status', '=', STATUS_ACTIVE)->first();
    }
    
    function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    
}
