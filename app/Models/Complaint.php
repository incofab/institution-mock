<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Complaint extends Model
{
    use HasFactory;
    
    protected $table = 'complaints';
    
    protected $fillable = [
        'user_id', 'category', 'complaint', 'resolved', 'phone', 'email', 'reply_count', 'last_reply_user_id'
    ];
    
    static function ruleCreate()
    {
        return [
            'complaint' => ['required', 'string' ],
            'category' => ['required', 'string' ],
        ];
    }
    
    static function ruleUpdate()
    {
        return [];
    }
    
    static function insert($post, User $user = null)
    {
        if(!($val = BaseModel::baseValidate($post, self::ruleCreate()))[SUCCESSFUL]) return $val;
        
        $post['user_id'] = $user->id ?? null;
        $post['complaint'] = htmlentities($post['complaint']);
        
//         if(!empty($post['phone'])){
//             $post['complaint'] .= " <br /><br /><b>Phone: {$post['phone']}</b>";
//         }
        
        if (static::create($post)){
            return [SUCCESSFUL => true, MESSAGE => 'Message received and will be addressed shortly'];
        }
        
        return [SUCCESSFUL => false, MESSAGE => 'Error: Data entry failed'];
    }
    
    static function markAsResolved($id)
    {
        $complaint = static::where('id', '=', $id)->first();
        
        $complaint['resolved'] = true;
        
        if($complaint->save()) return [SUCCESSFUL => true, MESSAGE => 'Complaint resolved'];
        
        return [SUCCESSFUL => false, MESSAGE => 'Error: Complaint resolution failed'];
    }
    
    static function getComplaintsCount($userId = null, $isResolved = false)
    {
        $sql = "SELECT COUNT(id) AS count_query FROM complaints ";
        
        $arr = [];
        
        if($userId) $arr['userId'] = $userId;
        
        $arr['resolved'] = $isResolved;
        
        $sql .= BaseModel::buildSqlQuery($arr);
        
        $superArray = BaseModel::pdoQuery($sql, $arr);
        
        return Arr::get($superArray, 'count_query', 0);
    }
    
    function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    function complaintReplies()
    {
        return $this->hasMany(ComplaintReply::class, 'complaint_id', 'id');
    }
    
}
