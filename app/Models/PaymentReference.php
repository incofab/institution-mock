<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @author Incofab
 * @property int $id
 * @property int $user_id
 * @property string $reference
 * @property string $status
 * @property string $content
 * @property string $merchant
 * @property string $payment_type
 * @property int $payment_id
 */
class PaymentReference extends Model
{
    protected $table = 'payment_references';
    
    public $fillable = ['id', 'user_id', 'reference', 'status', 'content', 'merchant', 
        'payment_type', 'payment_id', 'amount', 'charge'];
    
    const PAYMENT_TYPE_WALLET = 'Wallet Funding';
    const PAYMENT_TYPE_SUBSCRIPTION = 'Subscription Payment';
    
    static function ruleCreate()
    {
        return [
            'amount' => ['numeric', 'required'],
            'merchant' => ['required'],
            'reference' => ['required', 'unique:payment_references'],
        ];
    }
    
    static function ruleUpdate()
    {
        return self::ruleCreate();
    }
    
    /** Note: User ID can be null */
//     static function insert($userId, $reference, $merchant, $paymentOwnerId = null, $paymentType = Deposit::class, $content = null)
//     {
//         if (self::where('reference', '=', $reference)->first())
//         {
//             return ret(false, 'This reference already exists');
//         }
        
//         $arr = [];
//         $arr['reference'] = $reference;
//         $arr['user_id'] = $userId;
//         $arr['merchant'] = $merchant;
//         $arr['status'] = STATUS_PENDING;
//         $arr['payment_id'] = $paymentOwnerId;
//         $arr['payment_type'] = $paymentType;
//         $arr['content'] = json_encode($content);
        
//         $data = static::create($arr);
        
//         if ($data)
//         {
//             return [SUCCESSFUL => true, MESSAGE => 'Data recorded', 'data' => $data];
//         }
        
//         return ret(false, 'Error: Data entry failed');
//     }
    
    static function insert($userId, $post, $content = null)
    {
        if(!($val = BaseModel::baseValidate($post, self::ruleCreate()))[SUCCESSFUL]) return $val;
        
        $post['user_id'] = $userId;
        $post['status'] = STATUS_PENDING;
        $post['content'] = json_encode($content);
        
        $data = static::create($post);
        
        if ($data){
            return [SUCCESSFUL => true, MESSAGE => 'Data recorded', 'data' => $data];
        }
        
        return ret(false, 'Error: Data entry failed');
    }
    
    static function generateReferece($username)
    {
        $reference = $username.'-'.uniqid().'-'.mt_rand(0, 100);
        
        while (static::where('reference', '=', $reference)->first())
        {
            $reference = $username.'-'.uniqid().'-'.mt_rand(0, 100);
        }
        
        return $reference;
    }
    
    function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    // Returns subscription plan or Deposit (Though not implemented)
    function payment() {
        return $this->morphTo();
    }
}
