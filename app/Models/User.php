<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;
/**
 * @author Incofab
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property string $username
 * @property float $balance
 * @property int $pin_balance
 *
 */
class User extends \TCG\Voyager\Models\User
{
    use Notifiable, HasApiTokens, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'username'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    
    function isBalanceEnough($amount, $checkDB = false, $forPin = false)
    {
        $column = $forPin ? 'pin_balance' : 'balance';
        
        if(!$checkDB) return $this[$column] >= $amount;
        
        $userData = $this->where('id', '=', $this['id'])->first();
        
        return $userData[$column] >= $amount;
    }
    
    function debitUser($amount, $forPin = false)
    {
        $column = $forPin ? 'pin_balance' : 'balance';
        
        $userData = $this->where('id', '=', $this['id'])->first();
        
        $userData[$column] = $userData[$column] - $amount;
        
        $userData->save();
        
        $this[$column] = $userData[$column];
    }
    
    function creditUser($amount, $forPin = false)
    {
        $column = $forPin ? 'pin_balance' : 'balance';
        
        $userData = $this->where('id', '=', $this['id'])->first();
        
        $userData[$column] = $userData[$column] + $amount;
        
        $userData->save();
        
        $this[$column] = $userData[$column];
    }
    
    function createLoginToken() 
    {
        $loginTokenName = 'Login Token';
        
        // Delete old tokens, if any
//         $this->tokens()->where('name', '=', $loginTokenName)
//         ->where('tokenable_id', '=', $this->id)
//         ->where('tokenable_type', '=', User::class)->delete();
        
        return $this->createToken($loginTokenName)->plainTextToken;
    }
    
    function institutions() {
        return $this->belongsToMany(Institution::class);
    }
    
    function isAdmin() {
        return $this->email === 'admin@email.com';
    }
    
}
