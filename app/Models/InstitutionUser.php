<?php

namespace App\Models;

class InstitutionUser extends BaseModel
{
    protected $table = 'institution_user';
    
    protected $fillable = ['user_id', 'institution_id'];
    
    function institution()
    {
        return $this->belongsTo(Institution::class, 'institution_id', 'id');
    }
    
    function user() {
        return $this->belongsTo(User::class);
    }
}
