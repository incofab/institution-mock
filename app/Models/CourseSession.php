<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class CourseSession extends BaseModel
{
    use HasFactory;
    public $fillable = ['id', 'course_id', 'category', 'session', 'general_instructions', 'file_path', 'file_version'];
    
    static function ruleCreate()
    {
        return [
            'course_id' => ['required'],
        ];
    }
    
    static function ruleUpdate()
    {
        return [];
    }
    
    static function validateData($post)
    {
        return BaseModel::baseValidate($post, self::ruleCreate());
    }
    
    static function insert($post)
    {
        /** @var $val \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\Validation\Factory */
        $val = \Illuminate\Support\Facades\Validator::make($post, self::ruleCreate());
        
        if ($val->fails())
        {
            return [SUCCESSFUL => false, MESSAGE => 'Validation failed: '.getFirstValue($val->errors()->toArray()), 'val' => $val ];
        }
        
        $course = \App\Models\Course::where('id', '=', $post['course_id'])->first();
        
        if (!$course) return retF('Course detail not found');
        
        if (CourseSession::whereCourse_id($post['course_id'])->whereSession($post['session'])->first()){
            return retF("{$post['session']} session already exist for this course ID = {$post['course_id']}");
        }
        /*
        if(!empty($files['file_path']['name']))
        {
            $ret = $uploadPDF->uploadContent($files, 'file_path', $course['examContent']['exam_name'],
                $course['course_code'], $post['session']);
            
            if(!$ret[SUCCESSFUL]) return $ret;
            
            $post['file_path'] = $ret['file_path'];
            $post['file_version'] = 1;
        }
        */
        $created = static::create($post);
        
        if ($created)
        {
            // Create Questions table
//             $this->savePerQuestionsInstructions($post['course_id'], $created['id'], static::joinUpInstruction($post));
//             $this->savePassages($post['course_id'], $created['id'], static::joinUpPassage($post));
            
//             if(empty($course['is_file_content_uploaded']) && $post['file_path'])
//             {
//                 $course['is_file_content_uploaded'] = true;
//                 $course->save();
//             }
            
            return retS('Data recorded successfully');
        }
        
        return retF('Error: operation failed');
    }
    
    /**@deprecated*/
    static function edit($post, $files, \App\Core\UploadPDFContent $uploadPDF)
    {
        /** @var $val \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\Validation\Factory */
        $val = \Illuminate\Support\Facades\Validator::make($post, self::ruleCreate());
        
        if ($val->fails())
        {
            return [SUCCESSFUL => false, MESSAGE => 'Validation failed: '.getFirstValue($val->errors()->toArray()), 'val' => $val ];
        }
        
        $old = CourseSession::where('id', '=', $post['id'])
        ->with(['course'])->first();
        
        //Check if row exists
        if (!$old)
        {
            return [SUCCESSFUL => false, MESSAGE => 'There is no existing record, create new one', ];
        }
        
        $course = $old['course'];
        if(!empty($files['file_path']['name']))
        {
            $ret = $uploadPDF->uploadContent($files, 'file_path', $course['examContent']['exam_name'],
                $course['course_code'], $post['session']);
            
            if(!$ret[SUCCESSFUL]) return $ret;
            
            $old['file_path'] = $ret['file_path'];
            $old['file_version'] = $old['file_version']+1;
        }
        
        $old['course_id'] = $post['course_id'];
        $old['session'] = $post['session'];
        $old['category'] = $post['category'];
        $old['general_instructions'] = $post['general_instructions'];
        
        if ($old->save())
        {
            if(empty($course['is_file_content_uploaded']) && $old['file_path'])
            {
                $course['is_file_content_uploaded'] = true;
                $course->save();
            }
            
//             $this->savePerQuestionsInstructions($post['course_id'], $old['id'], static::joinUpInstruction($post));
//             $this->savePassages($post['course_id'], $old['id'], static::joinUpPassage($post));
            
            return [SUCCESSFUL => true, MESSAGE => 'Record updated successfully'];
        }
        
        return [SUCCESSFUL => false, MESSAGE => 'Error: Update failed'];
    }
    
    function savePerQuestionsInstructions($courseId, $sessionID, $post)
    {
        // First delete all the existing ones
        \App\Models\Instruction::where('course_session_id', '=', $sessionID)->delete();
        
        foreach ($post as $data) 
        {
            if(empty($data['instruction']) || empty($data['from_']) || empty($data['to_'])) continue;
            if(!is_numeric($data['from_']) || !is_numeric($data['to_'])) continue;
            
            \App\Models\Instruction::create([
                'course_id' => $courseId,
                'course_session_id'  => $sessionID,
                'instruction' => $data['instruction'],
                'from_' => $data['from_'],
                'to_'   => $data['to_'],
            ]);
            
        }
        
    }
    
    function savePassages($courseId, $sessionID, $post)
    {
        // First delete all the existing ones
        \App\Models\Passage::where('course_session_id', '=', $sessionID)->delete();
        
        foreach ($post as $data) 
        {
            if(empty($data['passage']) || empty($data['from_']) || empty($data['to_'])) continue;
            if(!is_numeric($data['from_']) || !is_numeric($data['to_'])) continue;
            
            \App\Models\Passage::create([
                'course_id' => $courseId,
                'course_session_id'  => $sessionID,
                'passage' => $data['passage'],
                'from_' => $data['from_'],
                'to_'   => $data['to_'],
            ]);
            
        }
        
    }
    
    static function joinUpInstruction($post) 
    {
        $arr = [];
        if(!isset($post['all_instruction'])) return $arr;
        
        $len = count(Arr::get($post['all_instruction'], 'instruction', []));
        
        for ($i = 0; $i < $len; $i++) {
            $arr[] = [
                'instruction' => $post['all_instruction']['instruction'][$i],
                'from_' => $post['all_instruction']['from_'][$i],
                'to_' => $post['all_instruction']['to_'][$i],
                'table_id' => Arr::get(Arr::get($post['all_instruction'], 'id'), $i),
            ];
        }
        return $arr;
    }
    
    static function joinUpPassage($post) 
    {
        $arr = [];
        if(!isset($post['all_passages'])) return $arr;
        
        $len = count(Arr::get($post['all_passages'], 'passage', []));
        
        for ($i = 0; $i < $len; $i++) 
        {
            $arr[] = [
                'passage' => $post['all_passages']['passage'][$i],
                'from_' => $post['all_passages']['from_'][$i],
                'to_' => $post['all_passages']['to_'][$i],
                'id' => Arr::get(Arr::get($post['all_passages'], 'id'), $i),
            ];
        }
        
        return $arr;
    }
    
    
    
    
    
    
    
    function course() {
        return $this->belongsTo(\App\Models\Course::class, 'course_id', 'id');
    }
    
    function questions() {
        return $this->hasMany(\App\Models\Question::class, 'course_session_id', 'id');
    }
    
    function instructions() {
        return $this->hasMany(\App\Models\Instruction::class, 'course_session_id', 'id');
    }
    
    function passages() {
        return $this->hasMany(\App\Models\Passage::class, 'course_session_id', 'id');
    }
    
}
