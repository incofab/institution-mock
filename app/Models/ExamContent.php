<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @author Incofab
 *
 *  @property int $id;
 *  @property string $country;
 *  @property string $region;
 *  @property string $institution;
 *  @property string $exam_name;
 *  @property string $fullname;
 *  @property boolean $is_file_content_uploaded;
 *  @property string $description;
 */
class ExamContent extends BaseModel
{
    use HasFactory;
    
    protected $table = 'exam_contents';
    
    protected $fillable = ['country', 'region', 'institution', 'exam_name', 'fullname', 
        'is_file_content_uploaded', 'description'
    ];

    static function ruleCreate()
    {
        return [
            'exam_name' => ['required', 'string', 'unique:exam_contents'],
        ];
    }
    
    static function ruleUpdate()
    {
        return [];
    }
    
    function insert($post)
    {
        /** @var $val \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\Validation\Factory */
        $val = \Illuminate\Support\Facades\Validator::make($post, self::ruleCreate());
        
        if ($val->fails())
        {
            return [SUCCESSFUL => false, MESSAGE => 'Validation failed: '.getFirstValue($val->errors()->toArray()), 'val' => $val ];
        }
        
        $created = static::create($post);
        
        if ($created)
        {
            return [SUCCESSFUL => true, MESSAGE => 'Data recorded successfully', 'data' => $created];
        }
        
        return [SUCCESSFUL => false, MESSAGE => 'Error: Data entry failed'];
    }
    
    function edit($post)
    {
        $val = \Illuminate\Support\Facades\Validator::make($post, self::ruleUpdate());
        
        if ($val->fails())
        {
            return [SUCCESSFUL => false, MESSAGE => 'Validation failed: '.getFirstValue($val->errors()->toArray()), 'val' => $val ];
        }
        
        $old = $this->where('id', '=', $post['id'])->first();
        
        //Check if row exists
        if (!$old)
        {
            return [SUCCESSFUL => false, MESSAGE => 'Record not found'];
        }
        
        if($old['exam_name'] != $post['exam_name'] &&
            $this->where('exam_name', '=', $post['exam_name'])->first())
        {
            return [SUCCESSFUL => false, MESSAGE => 'Error: Exam name already exist'];
        }
        
        $success = $this->where('id', '=', $post['id'])->update($post);
        
        if($success)
        {
            return [SUCCESSFUL => true, MESSAGE => 'Record updated successfully'];
        }
        
        return [SUCCESSFUL => false, MESSAGE => 'Error: Update failed'];
    }
    
    function courses()
    {
        return $this->hasMany(\App\Models\Course::class, 'exam_content_id', 'id');
    }
    
    
}
