<?php

namespace App\Models;

use Illuminate\Validation\Rule;
use Illuminate\Support\Arr;

class Transaction extends BaseModel
{
    protected $table = 'transactions';
    protected $guarded = ['*'];
    
    protected $fillable = [
        'amount', 'user_id', 'transaction_type', 'ip_address', 'choice_platform',
        'transaction_entry', 'status', 'bat', 'bbt', 'charge', 'comment', 'reference'
    ];
    
    // Note Objects are aalso used as transaction Type
    const TRANSACTION_TYPE_BANK_DEPOSIT = 'Bank Deposit';
    const TRANSACTION_TYPE_BANK_WITHDRAWAL = 'Bank Withdrawal';
    const TRANSACTION_TYPE_TRANSFER = 'Transfer';
    
    const TRANSACTION_ENTRY_CREDIT = 'Credit';
    const TRANSACTION_ENTRY_DEBIT = 'Debit';
    const TRANSACTION_ENTRY_TRANSFER = 'Transfer';
    
    const STATUSES = [STATUS_PENDING, STATUS_INVALID, STATUS_DELIVERED, STATUS_IN_USE,
        STATUS_CREDITED, STATUS_PAID, STATUS_CANCELLED, STATUS_TRANSFERED, STATUS_USED];
    
    const TRANSACTION_ENTRIES = [self::TRANSACTION_ENTRY_CREDIT,
        self::TRANSACTION_ENTRY_DEBIT, self::TRANSACTION_ENTRY_TRANSFER];
    
    const CHOICE_PLATFORMS = [CHOICE_PLATFORM_API, CHOICE_PLATFORM_WEBSITE,
        CHOICE_PLATFORM_APP
    ];
    
    static function ruleCreate() 
    {
        return [
            'amount' => ['required', 'numeric'],
            'transaction_type' => ['required', 'string' ],
            'status' => ['string', Rule::in(static::STATUSES)],
            'reference' => ['required'],
        ];
    }

    static function ruleUpdate() 
    {
        return [];
    }
    
    static function validate($post, User $user,
        \App\Interfaces\TransactionType $iTransactiontType)
    {
        /** @var $val \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\Validation\Factory */
        $val = \Illuminate\Support\Facades\Validator::make($post, self::ruleCreate());
        
        if ($val->fails())
        {
            return ret(false, 'Validation failed: '.getFirstValue($val->errors()->toArray()), null, $val);
        }
        
        if($prevTransaction = self::where('reference', '=', $post['reference'])->first())
        {
            return [SUCCESSFUL => false, IS_DUPLICATE => true,
                MESSAGE => 'Duplicate transaction', 'data' => $prevTransaction];
        }
        
        if($iTransactiontType)
        {
            $val2 = $iTransactiontType->validateFields($post, $user);
            
            if(!$val2[SUCCESSFUL])  return $val2;
        }
        
        return ret(true, 'Validated');
    }
    
    static function insert($post,
        User $user,
        \App\Interfaces\TransactionType $iTransactiontType
    ){
        $validate = self::validate($post, $user, $iTransactiontType);
        
        if(!$validate[SUCCESSFUL])  return $validate;
        
        $post['user_id'] = Arr::get($user, 'id');
        
        $transaction = static::create($post);
        
        if ($transaction)
        {
            $tTypeCreatedRet = $iTransactiontType->onTransactionCreated($post, $transaction, $user);
            
            return [
                SUCCESSFUL => true, MESSAGE => 'Transaction recorded',
                'data' => $transaction, 't_type_created' => Arr::get($tTypeCreatedRet, 'data'),
                'balance' => Arr::get($tTypeCreatedRet, 'balance')
            ];
        }
        
        return ret(false, 'Error: Data entry failed');
    }
    
    function getTransactionCount($userId = 'all', $transactionType = null, $status = null)
    {
        $arr = [];
        
        if($userId) $arr['user_id'] = $userId;
        
        if($transactionType) $arr['transaction_type'] = $transactionType;
        
        if($status && $status !== 'all') $arr[STATUS] = $status;
        
        $sql = "SELECT COUNT({$this->primaryKey}) AS query_result FROM {$this->getTable()}";
        
        $i = 0;
        foreach ($arr as $columnName => $value)
        {
            if($i === 0) $sql .= " WHERE ";
            else $sql .= " AND ";
            
            $sql .= " $columnName = :$columnName";
            
            $i++;
        }
        
        $superArray = $this->pdoQuery($sql, $arr);
        
        return Arr::get($superArray, 'query_result', 0);
    }
    
    function getTransactionSum(
        $userId = null, $transactionType = null,
        $status = null, $sumColumn = 'amount'
    ){
        $arr = [];
        
        if($userId) $arr['user_id'] = $userId;
        
        if($transactionType) $arr['transaction_type'] = $transactionType;
        
        if($status) $arr[STATUS] = $status;
        
        $sql = "SELECT SUM($sumColumn) AS query_result FROM {$this->getTable()}";
        
        $i = 0;
        foreach ($arr as $columnName => $value)
        {
            if($i === 0) $sql .= " WHERE ";
            
            else $sql .= " AND ";
            
            $sql .= " $columnName = :$columnName";
            
            $i++;
        }
        
        $superArray = $this->pdoQuery($sql, $arr);
        
        return Arr::get($superArray, 'query_result', 0);
    }
    
    /*
     function getDepositCount($userId)
     {
     $sql = "SELECT COUNT({$this->primaryKey}) AS count_users FROM {$this->getTable()}"
     . " WHERE customerId = :customerId AND transaction_entry = :transaction_entry";
     
     $arr = [
     ':transaction_entry' => self::TRANSACTION_ENTRY_CREDIT,
     
     ':customerId' => $userId
     ];
     
     $superArray = $this->pdoQuery($sql, $arr);
     
     return Arr::get($superArray, 'count_users', 0);
     }
     
     function getWithdrawalCount($userId)
     {
     $sql = "SELECT COUNT({$this->primaryKey}) AS count_users FROM {$this->getTable()}"
     . " WHERE customerId = :customerId AND transaction_entry = :transaction_entry";
     
     $arr = [
     ':transaction_entry' => self::TRANSACTION_ENTRY_DEBIT,
     
     ':customerId' => $userId
     ];
     
     $superArray = $this->pdoQuery($sql, $arr);
     
     return Arr::get($superArray, 'count_users', 0);
     }
     
     function getTransfersCount($userId)
     {
     $sql = "SELECT COUNT({$this->primaryKey}) AS count_users FROM {$this->getTable()}"
     . " WHERE customerId = :customerId AND transaction_entry = :transaction_entry";
     
     $arr = [
     ':transaction_entry' => self::TRANSACTION_ENTRY_TRANSFER,
     
     ':customerId' => $userId
     ];
     
     $superArray = $this->pdoQuery($sql, $arr);
     
     return Arr::get($superArray, 'count_users', 0);
     }
     
     function getUserTransactionsCount($userId)
     {
     $sql = "SELECT COUNT({$this->primaryKey}) AS count_query FROM {$this->getTable()}"
     . " WHERE customerId = :customerId";
     
     $arr = [
     ':customerId' => $userId
     ];
     
     $superArray = $this->pdoQuery($sql, $arr);
     
     return Arr::get($superArray, 'count_query', 0);
     }
     */
    
    function deposit()
    {
        return $this->hasOne(\App\Models\Deposit::class, 'transaction_id', 'id');
    }
    
    function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    
    
}
