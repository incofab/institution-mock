<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\TransactionType;

class Deposit extends BaseModel implements TransactionType
{
    protected $table = 'deposits';
    
    public $fillable = ['id', 'transaction_id', 'depositor_name', 'channel',
        'payment_method', 'reference'];
    
    static function ruleCreate()
    {
        return [
//             'depositor_name' => ['required', 'string', 'max:150'],
            'channel' => ['required', 'string'],
            'amount' => ['required', 'numeric'],
        ];
    }
    
    static function ruleUpdate()
    {
        return self::ruleCreate();
    }
    
    function validateFields($post, User $user = null)
    {   
//         /** @var $val \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\Validation\Factory */
//         $val = validator($post, self::ruleCreate());
        $val = \Illuminate\Support\Facades\Validator::make($post, self::ruleCreate());
        
        if ($val->fails())
        {
            return [SUCCESSFUL => false, MESSAGE => 'Validation failed: '.getFirstValue($val->errors()->toArray()), 'val' => $val,
                'error_msg' => json_encode($val->errors()->toArray()) ];
        }
        
        return ret(TRUE, '');
    }
    
    function onTransactionCreated($validatedData, Transaction $transaction, User $user)
    {
        $validatedData['transaction_id'] = $transaction->id;
        
        $created = self::create($validatedData);
        
        return [SUCCESSFUL => true, MESSAGE => 'Data recorded', 'data' => $created, 'balance' => $user['balance']];
    }
    
    
}
