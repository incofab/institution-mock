<?php

namespace App\Models;

class Institution extends BaseModel
{
    protected $fillable = ['added_by', 'code', 'name', 'address', 'phone', 'email', 'status'];
    
    static function insert($post)
    {
        $post['code'] = static::generateInstitutionCode();
        
        $data = static::create($post);
        
        if (!$data) return retF('Error: Data entry failed');
        
        return retS('Registration successful, You can login now', $data);
    }
    
    static function generateInstitutionCode()
    {
        $key = mt_rand(100000, 999999);
        
        while(Institution::whereCode($key)->first())
        {
            $key = mt_rand(100000, 999999);
        }
        
        return $key;
    }
    
    function courses()
    {
        return $this->hasMany(Course::class, 'institution_id', 'id');
    }
    
    function students()
    {
        return $this->hasMany(Student::class, 'institution_id', 'id');
    }
    
    function users() {
        return $this->belongsToMany(User::class);
    }
}
