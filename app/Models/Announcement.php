<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Announcement extends BaseModel
{
    use HasFactory;
    
    
    public $fillable = ['category', 'message', 'deleted', 'expired'];
    
    static function ruleCreate()
    {
        return [
            'message' => ['required', 'string'],
        ];
    }
    
    static function ruleUpdate()
    {
        $arr = self::ruleCreate();
        $arr['id'] = ['required', 'numeric'];
        return $arr;
    }
    
    static function insert($post)
    {
        if(!($val = BaseModel::baseValidate($post, self::ruleCreate()))[SUCCESSFUL]) return $val;
        
        static::create($post);
        
        return ret(TRUE, 'Data recorded');
    }
    
    static function edit($post)
    {
        if(!($val = BaseModel::baseValidate($post, self::ruleUpdate()))[SUCCESSFUL]) return $val;
        
        $success = static::where('id', '=', $post['id'])->update(
            ['message' => htmlentities($post['message'])]
        );
        
        if(!$success) return ret(false, 'Update failed');
        
        return ret(true, 'Record updated');
    }
    
    static function getMessages($lastId = 0)
    {
        return static::where('id', '>', $lastId)
        ->where('deleted', '=', false)
        ->where('expired', '=', false)
        ->orderBy('id', 'DESC')
        ->limit(30)
        ->get();
    }
    
    
}
