<?php

namespace App\Models;

class Student extends BaseModel
{
    public $fillable = ['id', 'user_id', 'institution_id', 'firstname', 'lastname', 'student_id', 'grade_id'];
    
    static function multiInsert($post, Institution $institution)
    {
        foreach ($post as $arr)
        {
            $arr['student_id'] = Student::generateStudentID();
            $arr['institution_id'] = $institution->id;
            
            Student::create($arr);
        }
        
        return retS('All records inserted');
    }
    
    static function insert($post, Institution $institution)
    {
        $post['student_id'] = Student::generateStudentID();
        $post['institution_id'] = $institution->id;
        
        $data = Student::create($post);
        
        if (!$data) return retF('Error: Data entry failed');
        
        $msg = 'Registration successful, You can login now';
        return retS($msg, $data);
    }
    
    static function edit($post)
    {
        $student = Student::whereId($post['id'])->firstOrFail();
        
        $student->update($post);
        
        return retS('Record updated successfully', $student);
    }
    
    static function generateStudentID()
    {
        $prefix = 'S-';
        
        $key = $prefix.rand(1000000, 9999999);
        
        while(Student::where('student_id', '=', $key)->first())
        {
            $key = $prefix.rand(1000000, 9999999);
        }
        
        return $key;
    }
    
    
    function grade() {
        return $this->belongsTo(Grade::class, 'grade_id', 'id');
    }
    
    function institution() {
        return $this->belongsTo(Institution::class, 'institution_id', 'id');
    }
    
}
