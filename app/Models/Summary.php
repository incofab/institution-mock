<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Summary extends BaseModel
{
    protected $fillable = ['course_id', 'chapter_no', 'title', 'description', 'summary'];
    
    static function ruleCreate()
    {
        return [
            'course_id' => ['required', 'numeric'],
            'chapter_no' => ['required', 'string'],
            'title' => ['required', 'string'],
            'summary' => ['required', 'string'],
        ];
    }
    
    static function ruleUpdate()
    {
        return [];
    }
    
    static function validateData($post)
    {
        return BaseModel::baseValidate($post, self::ruleCreate());
    }
    
    static function insert($post) 
    {
        /** @var $val \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\Validation\Factory */
        $val = \Illuminate\Support\Facades\Validator::make($post, self::ruleCreate());
        
        if ($val->fails())
        {
            return [SUCCESSFUL => false, MESSAGE => 'Validation failed: '.getFirstValue($val->errors()->toArray()), 'val' => $val ];
        }
        
        //Check if chapter_no already exists under the same courseName (chapter numbers must be unique
        if ($this->where('chapter_no', '=', $post['chapter_no'])
            ->where('course_id', '=', $post['course_id'])->first())
        {
            return [SUCCESSFUL => false, MESSAGE => 'This Chapter No already
					exists, Chapter Numbers must be unique'];
        }
        
        $created = static::create($post);
        
        if ($created)
        {
            return [SUCCESSFUL => true, MESSAGE => 'Data recorded successfully'];
        }
        
        return [SUCCESSFUL => false, MESSAGE => 'Error: Data entry failed'];
    }
    
    static function edit($post)
    {
        /** @var $val \Illuminate\Contracts\Validation\Validator|\Illuminate\Contracts\Validation\Factory */
        $val = \Illuminate\Support\Facades\Validator::make($post, self::ruleUpdate());
        
        if ($val->fails())
        {
            return [SUCCESSFUL => false, MESSAGE => 'Validation failed: '.getFirstValue($val->errors()->toArray()), 'val' => $val ];
        }
        
        $old = $this->where('id', '=', $post['id'])->first();
        
        //Check if row exists
        if (!$old)
        {
            return [SUCCESSFUL => false, MESSAGE => 'There is no existing record, create new one'];
        }
        
        $old['course_id'] = $post['course_id'];
        $old['chapter_no'] = $post['chapter_no'];
        $old['title'] = $post['title'];
        $old['description'] = Arr::get($post, 'description');
        $old['summary'] = $post['summary'];
        
        if ($old->save())
        {
            return [SUCCESSFUL => true, MESSAGE => 'Record updated successfully'];
        }
        
        return [SUCCESSFUL => false, MESSAGE => 'Error: Update failed'];
    }
    
    function course()
    {
        return $this->belongsTo(\App\Models\Course::class, 'course_id', 'id');
    }
    
}
