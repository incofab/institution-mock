<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Passage extends BaseModel
{
    protected $fillable = ['course_session_id', 'passage', 'from_', 'to_'];
    
    
    function session() {
        return $this->belongsTo(\App\Models\CourseSession::class, 'course_session_id', 'id');
    }
}
