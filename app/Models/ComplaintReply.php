<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComplaintReply extends Model
{
    use HasFactory;
    
    public $fillable = ['complaint_id', 'user_id', 'message'];
    
    static function ruleCreate()
    {
        return [
            'message' => [ 'required', 'string'],
        ];
    }
    
    static function ruleUpdate()
    {
        return self::ruleCreate();
    }
    
    static function insert($post, User $user, Complaint $complaint)
    {
        if(!($val = BaseModel::baseValidate($post, self::ruleCreate()))[SUCCESSFUL]) return $val;
        
        $post['coomplaint_id'] = $complaint->id;
        $post['user_id'] = $user->id;
        $ret = static::create($post);
        
        if ($ret){
            return [SUCCESSFUL => true, MESSAGE => 'Data recorded successfully', 'data' => $ret];
        }
        
        return [SUCCESSFUL => false, MESSAGE => 'Error: Data entry failed'];
    }
    
    function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    function complaint()
    {
        return $this->belongsTo(Complaint::class, 'complaint_id', 'id');
    }
    
    
}
