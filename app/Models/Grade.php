<?php
namespace App\Models;

class Grade extends BaseModel
{
    public $fillable = ['id', 'institution_id', 'title', 'description'];
    
    function institution() {
        return $this->belongsTo(Institution::class, 'institution_id', 'id');
    }
    
    function students() {
        return $this->hasMany(Student::class, 'grade_id', 'id');
    }
    
}
