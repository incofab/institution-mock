<?php
namespace App\Http\Controllers\Institution;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Grade;
use App\Models\Student;
use Illuminate\Support\Facades\Auth;
use App\Models\BaseModel;
use Illuminate\Http\Request;

class GradeController extends Controller
{
    private $gradeRepository;
    
    function __construct(\App\Helpers\GradeRepository $gradeRepository) 
    {
        $this->gradeRepository = $gradeRepository;
    }
    
    function index($institutionId) 
	{
        $ret = $this->gradeRepository->list($institutionId);
        
		return $this->view('institution.grade.index', [
			'allRecords' => $ret['all'],
		    'count' => $ret['count'],
		]);
	}

	function create($institutionId)
	{
		return $this->view('institution.grade.create');
	}

	function store($institutionId, Request $request) 
	{
	    $ret = $this->gradeRepository->create($request->all());
	    
	    if(!$ret[SUCCESSFUL]) return $this->redirect(redirect()->back(), $ret);
	    
	    return redirect(route('institution.grade.create', [$institutionId]))
	    ->with('message', $ret[MESSAGE]);
	}

	function edit($institutionId, Grade $grade) 
	{
		return $this->view('institution.grade.edit', ['data' => $grade]);
	}
	
	function update($institutionId, Request $request, Grade $grade) 
	{
	    $ret = $this->gradeRepository->update($grade, $request->all());
	    
	    if(!$ret[SUCCESSFUL]) return $this->redirect(redirect()->back(), $ret);
	    
	    return redirect(route('institution.grade.index', $institutionId))->with('message', $ret[MESSAGE]);
	}
	
	function delete($institutionId, $table_id) 
	{
	    $ret = $this->gradeRepository->delete($table_id);
	    
	    if(!$ret[SUCCESSFUL]) return $this->redirect(redirect()->back(), $ret);
	    
	    return redirect(route('institution.grade.index', $institutionId))->with('message', $ret[MESSAGE]);
	}

	
}





