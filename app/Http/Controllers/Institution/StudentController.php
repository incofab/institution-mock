<?php
namespace App\Http\Controllers\Institution;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Student;
use Illuminate\Support\Facades\Auth;
use App\Models\BaseModel;
use Illuminate\Http\Request;
use App\Helpers\GradeRepository;
use App\Models\Grade;

class StudentController extends Controller
{
    private $gradeRepository;
    
    function __construct(GradeRepository $gradeRepository)
    {
        $this->gradeRepository = $gradeRepository;
    }
    
    function index($institutionId, $gradeId = null) 
	{   
	    $allRecords = Student::where('institution_id', '=', $institutionId);
	    
	    if ($gradeId) $allRecords = $allRecords->whereGrade_id($gradeId);
	        
	    $allRecords = $allRecords->orderBy('id', 'DESC')->take($this->numPerPage)->get();
		
		return $this->view('institution.student.index', [
			'gradeId' => $gradeId,
		    'allGrades' => $this->gradeRepository->list($institutionId)['all'],
			'allRecords' => $allRecords,
		    'count' => BaseModel::getCount('students', ['institution_id' => $institutionId]),
		]);
	}

	function create($institutionId)
	{
	    return $this->view('institution.student.create', [
	        'allGrades' => $this->gradeRepository->list($institutionId)['all'],
		]);
	}
    
	function store($institutionId, Request $request) 
	{
	    $ret = Student::insert($request->all(), $request->get('institution'));
	    
	    if(!$ret[SUCCESSFUL]) return $this->redirect(redirect()->back(), $ret);
	    
	    return redirect(route('institution.student.index', [$institutionId]))
	    ->with('message', 'Student registered, Record exam subjects');
	}

	function multiStudentCreate($institutionId)
	{
	    return $this->view('institution.student.multi-create', [
	        'allGrades' => $this->gradeRepository->list($institutionId)['all'],
		]);
	}
    
	function multiStudentStore($institutionId, Request $request)
	{
	    $students = $request->input('students');
	    
	    foreach ($students as $student) 
	    {
    	    $ret = Student::insert($student, $request->get('institution'));
	    }
	    
	    return redirect(route('institution.student.index', [$institutionId]))
	    ->with('message', 'Students registered');
	}

	function edit($institutionId, $tableId) 
	{
	    $student = Student::whereId($tableId)->firstOrFail();
	    
		return $this->view('institution.student.edit', [
		    'data' => $student,
		    'allGrades' => $this->gradeRepository->list($institutionId)['all'],
		]);
	}
	
	function update($institutionId, Request $request, Student $student) 
	{
	    $student->update($request->all());
	    
	    return redirect(route('institution.student.index', $institutionId))
	    ->with('message', "{$student->firstname}'s record updated");
	}
	
	function suspend($institutionId, $table_id) 
	{
	    Student::whereId($table_id)->whereInstitution_id($institutionId)
			->update(['status' => STATUS_SUSPENDED]);
		
		return redirect(route('institution.student.index', $institutionId))->with('message', '');
	}

	function unSuspend($institutionId, $table_id) 
	{
	    Student::whereId($table_id)->whereInstitution_id($institutionId)
	    ->update(['status' => STATUS_ACTIVE]);
	    
	    return redirect(route('institution.student.index', $institutionId))->with('message', 'Student has been unsuspended');
	}
	
	function show($institutionId, $table_id_or_studentId) 
	{
	    $studentData = Student::whereId($table_id_or_studentId)
	    ->whereInstitution_id($institutionId)
	    ->orWhere('student_id', '=', $table_id_or_studentId) 
	    ->whereInstitution_id($institutionId)->first();
		
		return $this->view('institution.student.show', [
		    'studentData' => $studentData
		]);
	}

	function destroy($institutionId, $table_id) 
	{
	    Student::whereId($table_id)
	    ->whereInstitution_id($institutionId)
	    ->delete();
	    
	    return redirect(route('institution.student.index', $institutionId))->with('message', 'Student deleted successfully');
	}

	function multiDelete($institutionId, Request $request) 
	{
	    $studentIDs = explode(',', $request->input('student_id'));

	    if(empty($studentIDs)){
	        return redirect(route('institution.student.index', $institutionId))->with('error', 'No student selected');
		}
	    
		$builder = Student::whereInstitution_id($institutionId)
		  ->whereIn('student_id', $studentIDs);
	    
	    $builder->delete();
	    
	    return redirect(route('institution.student.index', $institutionId))->with('message', 'Students deleted');
	}

	function uploadStudentsView($institutionId)
	{
        return  $this->view('institution.student.upload', [
            'grades' => Grade::whereInstitution_id($institutionId)->get(),
        ]);
	}

	function uploadStudents($institutionId, Request $request, \App\Helpers\StudentsUploadHelper $studentsUploadHelper)
	{
        $ret = $studentsUploadHelper->uploadStudent($_FILES, $request->get('institution'));
		
        if(!$ret[SUCCESSFUL]) return $this->redirect(redirect()->back(), $ret);
        
        return redirect(route('institution.student.index', $institutionId))->with('message', $ret[MESSAGE]);
    }

	function downloadSampleExcel($institutionId)
	{
	    $fileToDownload = public_path()."/student-recording-template.xlsx";
	    $file_name = 'student-recording-template.xlsx';
	    
	    header("Content-Type: application/zip");
	    
	    header("Content-Disposition: attachment; filename=$file_name");
	    
	    header("Content-Length: " . filesize($fileToDownload));
	    
	    readfile($fileToDownload);
	    
	    exit();
	}
	
}





