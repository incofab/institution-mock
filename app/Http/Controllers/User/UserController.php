<?php
namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\InstitutionUser;

class UserController extends Controller
{
    
	function index() {
	    
	    $user = Auth::user();
	    
	    if($user->isAdmin()) return redirect(route('admin.dashboard'));
	    
	    $institutionUser = InstitutionUser::whereUser_id($user->id)->first();
	    
	    if($institutionUser) return redirect(route('institution.dashboard', $institutionUser->institution->id));
	    
		return $this->view('user.index', []);
	}
	
}





