<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use App\Models\InstitutionUser;
use App\Models\Institution;

class VerifyInstitutionUser {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        
        $user = $request->user();
        
        if (!$user){
            
            $message = 'You are not looged in.';
            
            return $request->expectsJson()
            ? abort(403, $message)
            : Redirect::guest(URL::route('login'));
        }
        
        $instId = $request->route()->parameter('institution_id');
        
        $institutionUser = InstitutionUser::whereUser_id($user->id)
        ->whereInstitution_id($instId)->with('institution')->first();
        
        if (!$institutionUser) 
        {
            if(!$user->isAdmin())
            {
                $message = 'You are not authorized to access this page.';
                
                return $request->expectsJson()
                ? abort(403, $message)
                : Redirect::guest(URL::route('login'))->with('error', $message);
            }
            else 
            {
                $institution = Institution::whereId($instId)->firstOrFail();
            }
        }
        else
        {
            $institution = $institutionUser->institution;
        }
        
        
        View::share('institution', $institution);
        
        $request->merge([
            'institution' => $institution,
            'institution_id' => $institution->id,
        ]);
        
        return $next($request);
    }

}
